package com.example.azarov

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(this, "HELLO!", Toast.LENGTH_LONG)
        setContentView(R.layout.activity_main)
    }
}